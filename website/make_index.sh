#!/bin/bash
# BASH script designed to generate index files using 
# Twitter Bootstrap [getbootstrap.com]

# scripts from https://blog.sleeplessbeastie.eu/2013/11/21/how-to-generate-directory-index-files/

# Notes:
# * Script will ignore "img" and "css" directories and "index.html" file
# * Remember to copy Twitter Bootstrap files (css,img)

# Sample usage:
# cd /var/www/download
# make_index.sh

# web root directory, 
# it will be used to generate URLs
# ""           - root directory
# "/download" - '/download' subdirectory
root_directory="."

# template settings
brand="Formation CI/CD"
title="Formation CI/CD - Root"

# overide existing index.html file possible values 0 or 1
overide_existing_index=0

# generate header part of the HTML file
function header {
  cat << EOF
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>$title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
  </head>

  <body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <!-- Brand -->
	  <a class="navbar-brand" href="/">
	  	<img src="/image/logo.png" alt="Logo" style="width:100px;">
	  </a>
	</nav>  

    <div class="container"> 
      <div class="page-header"><h1>$title</h1></div>
EOF
}

# generate footer part of the HTML file
function footer {
  cat << EOF
    </div> <!-- /container -->
  </body>
</html>
EOF
}


# create index file for each directory (ignore ./css and ./img directories)
find . -type d -not -path "*/img" -not -path "*/css" | while read directory; do
  # remove "./" from the beginning of the path
  directory=${directory#./}
  
  # index file
  index_file=$directory/index.html

  if [ -f "$index_file" -a $overide_existing_index == "0" ]; then
     echo "skipping generation of $index_file [already exists]"
  else 

    # create/truncate index file
    : > $index_file

    # append header to the index file
    header >> $index_file

    # append "Up to higher level directory" link
    echo "<div class=\"row\">" >> $index_file
    if [ -f "$directory/.content_desc.html" ]; then
      folder_description=$(cat "$directory/.content_desc.html")
    fi
    if [ "$directory" != "." ]; then
      echo "<div class=\"col-sm-4\"><h2 class=\"muted\"><i class=\"far fa-folder-open\"></i> ${directory}</h2></div><div class=\"col-sm-8\">$folder_description</div>" >> $index_file    
      echo "<div class=\"col-sm-12\"><hr/></div>" >> $index_file
      echo "<div class=\"col-sm-12\"><i class=\"fas fa-arrow-up\"></i> <a href=\"../index.html\">Up to higher level directory</a></div>" >> $index_file
    else
      echo "<div class=\"col-sm-4\"><h2 class=\"muted\"><i class=\"far fa-folder-open\"></i> root directory</h2></div><div class=\"col-sm-8\">$folder_description</div>" >> $index_file
      echo "<div class=\"col-sm-12\"><hr/></div>" >> $index_file
    #  echo "<i class=\"fas fa-arrow-up\"></i> Up to higher level directory" >> $index_file
    fi
    echo "</div>" >> $index_file

    # append empty row
    echo "<div class=\"row\"><div class=\"col-sm-12\"><br/></div></div>" >> $index_file

    # generate content
    content=""
    while read file
    do
      if [ -d "$directory/$file" ]; then
        content+="<div class=\"row\">"
        #if [ "$directory" == "." ]; then
          echo $index_file $root_directory/$file/index.html
          content+="<div class=\"col-sm-10\"><div class=\"directory\"><i class=\"fas fa-arrow-right\"></i> <a href=\"$root_directory/$file/index.html\">$file</a></div></div>"
        #else
        #  content+="<div class=\"col-sm-10\"><div class=\"directory\"><i class=\"fas fa-arrow-right\"></i> <a href=\"$root_directory/$directory/$file/index.html\">$file</a></div></div>"
        #fi    
        content+="<div class=\"col-sm-2\"><div class=\"type\"><i class=\"far fa-folder\"></i> Directory</div></div>"
        content+="</div>"
        content+="<hr/>"            
      elif [ -f "$directory/$file" ]; then
        # file_type=$(file -b "$directory/$file")
        file_size=$(du -h "$directory/$file" | cut -f1)
        file_date=$(date -I -r "$directory/$file")
        content+="<div class=\"row\">"
        #if [ "$directory" == "." ]; then
          content+="<div class=\"col-sm-8\"><div class=\"file\"><a href=\"$root_directory/$file\">$file</a></div></div>"
        #else
        #  content+="<div class=\"col-sm-8\"><div class=\"file\"><a href=\"$root_directory/$directory/$file\">$file</a></div></div>"
        #fi    
        content+="<div class=\"col-sm-2\"><div class=\"type\"> $file_date </div></div>"  
        content+="<div class=\"col-sm-2\"><div class=\"type\"><i class=\"far fa-hdd\"></i> $file_size</div></div>"     
        content+="</div>"      
        content+="<div class=\"row\">"            
        # content+="<div class=\"col-sm-12\"><div class=\"type\"><i class=\" icon-info-sign\"></i> $file_type</div></div>"                  
        content+="</div>"      
        content+="<hr/>"      
      fi
    done < <(ls -1 --group-directories-first $directory | grep -v "index.html\|css\|img\|make_index.sh")

    # append content to the index file
    echo $content >> $index_file

    # append footer to the index file
    footer >> $index_file
  fi
done
